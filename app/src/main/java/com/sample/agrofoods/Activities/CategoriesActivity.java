package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Adapters.AllCategoriesRecyclerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.AllCategoriesResponse;
import com.sample.agrofoods.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sample.agrofoods.Utilities.capitalize;


public class CategoriesActivity extends AppCompatActivity {

    @BindView(R.id.recycler_categories)
    RecyclerView recycler_categories;
    AppController appController;
   // Integer[] imageId = {R.drawable.banner, R.drawable.banner2, R.drawable.banner, R.drawable.banner3};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(capitalize("Categories"));
        appController = (AppController) getApplication();

        if (appController.isConnection()) {
            getData();
        }
        else {
            setContentView(R.layout.internet);
            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                  //  overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
    }

    private void getData() {

        ProgressDialog progressDialog=new ProgressDialog(CategoriesActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<AllCategoriesResponse> call= RetrofitClient.getInstance().getApi().AllCategories();
        call.enqueue(new Callback<AllCategoriesResponse>() {
            @Override
            public void onResponse(Call<AllCategoriesResponse> call, Response<AllCategoriesResponse> response) {
                if (response.isSuccessful());
                AllCategoriesResponse allCategoriesResponse=response.body();
                if (allCategoriesResponse.getCode() == 200){
                    progressDialog.dismiss();
                    List<AllCategoriesResponse.DataBean> dataBeanList=allCategoriesResponse.getData();
                    final AllCategoriesRecyclerAdapter adapter = new AllCategoriesRecyclerAdapter(getApplicationContext(), dataBeanList);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                    recycler_categories.setLayoutManager(mLayoutManager);
                    recycler_categories.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(4), true));
                    recycler_categories.setItemAnimator(new DefaultItemAnimator());
                    recycler_categories.setAdapter(adapter);
                }
                else if (allCategoriesResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(CategoriesActivity.this, "", Toast.LENGTH_SHORT).show();
                }
                else if (allCategoriesResponse.getCode() == 204){
                    progressDialog.dismiss();
                    Toast.makeText(CategoriesActivity.this, "No Data Found!..", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllCategoriesResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CategoriesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                startActivity(new Intent(CategoriesActivity.this,DashBoard.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(CategoriesActivity.this,DashBoard.class));
    }
}
