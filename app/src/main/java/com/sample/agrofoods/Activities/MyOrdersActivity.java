package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Adapters.MyOrdersAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.MyOrdersResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrdersActivity extends AppCompatActivity {
    int color = Color.RED;
    AppController appController;
    @BindView(R.id.orderidRecycler)
    RecyclerView orderidRecycler;
    @BindView(R.id.txtError)
    TextView txtError;

    PrefManager pref;
    MyOrdersAdapter myOrderAdapter;
    //List<OrderIdItem> orderIdItemList = new ArrayList<>();
    String UserID, orderId,token;
    String userId,tokenValue,deviceId;
    boolean checkoutStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_id_actvity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Orders");
        appController = (AppController) getApplication();

        pref = new PrefManager(MyOrdersActivity.this);
        HashMap<String, String> profile = pref.getUserDetails();
        token = profile.get("Token");

        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }

        if (appController.isConnection()) {

            prepareOrderIdData(token);

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                  //  overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

        }
    }


    private void prepareOrderIdData(String token) {

        ProgressDialog progressDialog=new ProgressDialog(MyOrdersActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<MyOrdersResponse> call= RetrofitClient.getInstance().getApi().MyOrders(token);
        call.enqueue(new Callback<MyOrdersResponse>() {
            @Override
            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response) {
                if (response.isSuccessful());
                MyOrdersResponse myOrdersResponse=response.body();

                if (myOrdersResponse.getCode() == 200){
                    progressDialog.dismiss();

                    List<MyOrdersResponse.DataBean> ordersBeanList=myOrdersResponse.getData();

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    orderidRecycler.setLayoutManager(layoutManager);
                    orderidRecycler.setItemAnimator(new DefaultItemAnimator());

                    myOrderAdapter = new MyOrdersAdapter(MyOrdersActivity.this,ordersBeanList);
                    orderidRecycler.setAdapter(myOrderAdapter);
                    myOrderAdapter.notifyDataSetChanged();
                }
                else if (myOrdersResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(MyOrdersActivity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (myOrdersResponse.getCode() == 204){
                    progressDialog.dismiss();
                    Toast.makeText(MyOrdersActivity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<MyOrdersResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyOrdersActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;


        }

        return super.onOptionsItemSelected(item);
    }
}
