package com.sample.agrofoods.Activities;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Adapters.LocalServiceAdapter;
import com.sample.agrofoods.Models.LocalServiceResponse;
import com.sample.agrofoods.R;
import androidx.recyclerview.widget.LinearLayoutManager;

public class LocalServicesActivity extends AppCompatActivity {
    RecyclerView recycler_localservices;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.localservices_activity);

        recycler_localservices=findViewById(R.id.recycler_localservices);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Local Services");

        LocalServiceResponse[] localServiceResponse = new LocalServiceResponse[]{
                new LocalServiceResponse(R.drawable.service,"S.SHAILENDER","9700766313","Interior Designer-Bhagavan Plasters","9573322725","Undertakes all P.O.P, Gypsum & Wooden Fall Celling Works"),
                new LocalServiceResponse(R.drawable.service1,"J.GANESH","9848942022","Electricians","xxxxxxxxxx","Undertakes  Home Wiring Works install and maintain all of the electrical and power systems for our homes, businesses, and factories")
        };

        LocalServiceAdapter localServiceAdapter = new LocalServiceAdapter(LocalServicesActivity.this,localServiceResponse);
        recycler_localservices.setLayoutManager(new LinearLayoutManager(LocalServicesActivity.this, LinearLayoutManager.VERTICAL, false));
        recycler_localservices.setHasFixedSize(true);
        recycler_localservices.setNestedScrollingEnabled(false);
        recycler_localservices.setAdapter(localServiceAdapter);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
