package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.sample.agrofoods.Adapters.ProductsRecyclerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.AllProductsResponse;
import com.sample.agrofoods.Models.CartCountResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductListActivity extends AppCompatActivity {
    SwipeRefreshLayout swipeRefreshLayout;
    AppController appController;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.text_count)
    TextView textCount;
    @BindView(R.id.relative_count)
    RelativeLayout relativeCount;
    //  private ProductAadpter adapter;
    private RecyclerView rcProduct;
    LinearLayout sortLinear, filterLinear;
    // private PrefManager pref;
    String userid, title, module, catId, subcatId, tokenValue, deviceId;
    int cartindex;
    String positionValue;
    PrefManager prefManager;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefManager = new PrefManager(ProductListActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (getIntent().getExtras() != null) {
            catId = getIntent().getStringExtra("catId");
            subcatId = getIntent().getStringExtra("subcatId");
            title = getIntent().getStringExtra("title");

        }

        txtTitle.setText(title);

        // init SwipeRefreshLayout and ListView
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.simpleSwipeRefreshLayout);
        //    swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);

        rcProduct = (RecyclerView) findViewById(R.id.rcProduct);

        appController = (AppController) getApplication();

        if (appController.isConnection()) {
            CartCount(token);
            prepareProductData(catId);

                        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    CartCount(token);
                    prepareProductData(catId);
                    swipeRefreshLayout.setRefreshing(false);

                }
            });


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ProductListActivity.this, DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

        }

    }

    private void prepareProductData(String catId) {

        ProgressDialog progressDialog = new ProgressDialog(ProductListActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<AllProductsResponse> call = RetrofitClient.getInstance().getApi().ProductListing(catId);
        call.enqueue(new Callback<AllProductsResponse>() {
            @Override
            public void onResponse(Call<AllProductsResponse> call, Response<AllProductsResponse> response) {
                if (response.isSuccessful()) ;
                AllProductsResponse allProductsResponse = response.body();

                try {
                    if (allProductsResponse.getCode() == 200) {
                        progressDialog.dismiss();

                        List<AllProductsResponse.DataBean> productsBeanList = allProductsResponse.getData();
                        final ProductsRecyclerAdapter adapter = new ProductsRecyclerAdapter(getApplicationContext(), productsBeanList);
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                        rcProduct.setLayoutManager(mLayoutManager);
                        rcProduct.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(2), true));
                        rcProduct.setItemAnimator(new DefaultItemAnimator());
                        rcProduct.setAdapter(adapter);

                    } else if (allProductsResponse.getCode() == 401) {
                        progressDialog.dismiss();
                        Toast.makeText(ProductListActivity.this, allProductsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (allProductsResponse.getCode() == 204) {
                        progressDialog.dismiss();
                        Toast.makeText(ProductListActivity.this, allProductsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<AllProductsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProductListActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    protected void onRestart() {
        CartCount(token);

        super.onRestart();
    }

    @Override
    protected void onStart() {
        CartCount(token);

        super.onStart();
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
//        final MenuItem menuItem = menu.findItem(R.id.action_cart);
//       menuItem.setIcon(Converter.convertLayoutToImage(ProductListActivity.this, cartindex, R.drawable.ic_actionbar_bag));
//        return true;
//    }

    public void CartCount(String token) {

        Call<CartCountResponse> call = RetrofitClient.getInstance().getApi().CartCount(token);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) {
                    CartCountResponse cartCountResponse = response.body();

                    if (cartCountResponse.getCode() == 200) {
                        CartCountResponse.DataBean dataBean = cartCountResponse.getData();
                        cartindex = dataBean.getCount();
                        Log.e("CART_INDEX", "" + cartindex);

                        if (String.valueOf(cartindex).equals("0")) {
                            relativeCount.setVisibility(View.GONE);
                        } else {
                            relativeCount.setVisibility(View.VISIBLE);
                        }

                        textCount.setText(String.valueOf(cartindex));

                    } else if (cartCountResponse.getCode() == 204) {
                        Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Server error...", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @OnClick({R.id.img_back, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_cart:
                Intent intent = new Intent(ProductListActivity.this, CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//
//            case android.R.id.home:
//                onBackPressed();
//                break;
//
//            case R.id.action_cart:
//                Intent intent = new Intent(ProductListActivity.this, CartActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//             //   overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                break;
////
////            case R.id.action_search:
////                Intent intent1 = new Intent(ProductListActivity.this, SearchActivity.class);
////                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                startActivity(intent1);
////                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
