package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.sample.agrofoods.Adapters.SubSubCategoriesListRecyclerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.CartCountResponse;
import com.sample.agrofoods.Models.SubCategoriesResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubSubCatActivity extends AppCompatActivity {
    SwipeRefreshLayout swipeRefreshLayout;
    AppController appController;
    @BindView(R.id.txtError)
    TextView txtError;
    //  private ProductAadpter adapter;
    @BindView(R.id.rcProduct)
    RecyclerView rcProduct;
    LinearLayout sortLinear, filterLinear;
    // private PrefManager pref;
    String userid, title, module, catId, subcatId, tokenValue, deviceId;
    int cartindex;
    String positionValue;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.text_count)
    TextView textCount;
    @BindView(R.id.relative_count)
    RelativeLayout relativeCount;
    private PrefManager prefManager;
    String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_sub_cat_activity);

        ButterKnife.bind(this);


        prefManager = new PrefManager(SubSubCatActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (getIntent().getExtras() != null) {
            catId = getIntent().getStringExtra("catId");
            title = getIntent().getStringExtra("title");

        }
        txtTitle.setText(title);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.simpleSwipeRefreshLayout);

        appController = (AppController) getApplication();
        if (appController.isConnection()) {
            CartCount(token);
            prepareProductData(catId);

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    CartCount(token);
                    prepareProductData(catId);

                    swipeRefreshLayout.setRefreshing(false);

                }
            });


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(SubSubCatActivity.this, DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
//                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }


    }

    private void prepareProductData(String catId) {


        ProgressDialog progressDialog = new ProgressDialog(SubSubCatActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<SubCategoriesResponse> call = RetrofitClient.getInstance().getApi().Subcategory(catId);
        call.enqueue(new Callback<SubCategoriesResponse>() {
            @Override
            public void onResponse(Call<SubCategoriesResponse> call, Response<SubCategoriesResponse> response) {
                if (response.isSuccessful()) ;
                SubCategoriesResponse subCategoriesResponse = response.body();
                if (subCategoriesResponse.getCode() == 200) {
                    progressDialog.dismiss();
                    List<SubCategoriesResponse.DataBean> dataBeanList = subCategoriesResponse.getData();
                    SubSubCategoriesListRecyclerAdapter subCategoriesRecyclerAdapter = new SubSubCategoriesListRecyclerAdapter(dataBeanList, getApplicationContext(), catId);
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    rcProduct.setLayoutManager(mLayoutManager1);
                    rcProduct.setItemAnimator(new DefaultItemAnimator());
                    rcProduct.setHasFixedSize(true);
                    rcProduct.setNestedScrollingEnabled(false);
                    rcProduct.setAdapter(subCategoriesRecyclerAdapter);
                } else if (subCategoriesResponse.getCode() == 204) {
                    progressDialog.dismiss();
                    Toast.makeText(SubSubCatActivity.this, "No Data...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubCategoriesResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(SubSubCatActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    protected void onRestart() {
        CartCount(token);
        super.onRestart();
    }

    @Override
    protected void onStart() {
        CartCount(token);
        super.onStart();
    }

    @OnClick({R.id.img_back, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                break;
            case R.id.img_cart:
                break;
        }
    }

    public void CartCount(String token) {

        Call<CartCountResponse> call = RetrofitClient.getInstance().getApi().CartCount(token);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) {
                    CartCountResponse cartCountResponse = response.body();

                    if (cartCountResponse.getCode() == 200) {
                        CartCountResponse.DataBean dataBean = cartCountResponse.getData();
                        cartindex = dataBean.getCount();
                        Log.e("CART_INDEX", "" + cartindex);

                        if (String.valueOf(cartindex).equals("0")) {
                            relativeCount.setVisibility(View.GONE);
                        } else {
                            relativeCount.setVisibility(View.VISIBLE);
                        }

                        textCount.setText(String.valueOf(cartindex));

                    } else if (cartCountResponse.getCode() == 204) {
                        Toast.makeText(getApplicationContext(), cartCountResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Server error...", Toast.LENGTH_SHORT).show();
            }
        });


    }

//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
//        final MenuItem menuItem = menu.findItem(R.id.action_cart);
//         menuItem.setIcon(Converter.convertLayoutToImage(SubSubCatActivity.this, cartindex, R.drawable.ic_actionbar_bag));
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//
//            case android.R.id.home:
//                onBackPressed();
//                break;
//
//            case R.id.action_cart:
//                Intent intent = new Intent(SubSubCatActivity.this, CartActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                //   overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                break;
////
////            case R.id.action_search:
////                Intent intent1 = new Intent(ProductListActivity.this, SearchActivity.class);
////                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                startActivity(intent1);
////                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
