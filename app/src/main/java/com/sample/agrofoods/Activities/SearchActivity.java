package com.sample.agrofoods.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.sample.agrofoods.Adapters.SearchAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.ProductSearchResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends Activity {

    @BindView(R.id.search)
    SearchView search;
    @BindView(R.id.rcProduct)
    RecyclerView rcProduct;
    @BindView(R.id.txtNodata)
    TextView txtNodata;
    @BindView(R.id.txtSearch)
    TextView txtSearch;
    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private SearchAdapter adapter;

    private PrefManager pref;
    String userid,tokenValue,deviceId;
    AppController appController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        appController = (AppController) getApplicationContext();
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchActivity.this,DashBoard.class));
            }
        });

        search.setActivated(true);
        search.setQueryHint("Type your keyword here");
        search.onActionViewExpanded();
        search.setIconified(false);


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                Log.e("QUERY :", "" + query);

                prepareProductData(query,"");

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (search.getQuery().length() == 0) {

                    prepareProductData(query,"");

                }
                return false;
            }
        });


        search.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                search.setQuery("", false);
                adapter.notifyDataSetChanged();
                prepareProductData("","HideRecycler");
                return false;
            }
        });

    }

    private void prepareProductData(final String query, final String hideRecycler) {

        String searchQuery = "";

        try {
            searchQuery = URLEncoder.encode(query, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String finalSearchQuery = searchQuery;

        ProgressDialog progressDialog = new ProgressDialog(SearchActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<ProductSearchResponse> call= RetrofitClient.getInstance().getApi().ProductSearch(finalSearchQuery);
        call.enqueue(new Callback<ProductSearchResponse>() {
            @Override
            public void onResponse(Call<ProductSearchResponse> call, Response<ProductSearchResponse> response) {
                if (response.isSuccessful());
                ProductSearchResponse productSearchResponse=response.body();
                if (productSearchResponse.getCode() == 204){
                    progressDialog.dismiss();

                    txtSearch.setVisibility(View.VISIBLE);
                    txtNodata.setVisibility(View.GONE);
                    rcProduct.setVisibility(View.VISIBLE);
                    txtSearch.setText("Search results of " + query);

                    List<ProductSearchResponse.DataBean> docsBeanList=productSearchResponse.getData();

                    if (hideRecycler.equalsIgnoreCase("HideRecycler")){
                        rcProduct.setVisibility(View.GONE);
                    }
                    else {
                        // homeKitchen Adapter
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                        rcProduct.setLayoutManager(mLayoutManager);
                        rcProduct.setItemAnimator(new DefaultItemAnimator());


                        adapter = new SearchAdapter(getApplicationContext(), docsBeanList);
                        rcProduct.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }

                }
                else if (productSearchResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(SearchActivity.this, productSearchResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductSearchResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(SearchActivity.this, "Server Error..", Toast.LENGTH_SHORT).show();
            }
        });


    }


    // validate name
    private boolean isValidQuery(String name) {

         if (name.length() < 3 ) {

            return false;
        }else {
             prepareProductData(name, "HideRecycler");
         }
        return true;
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SearchActivity.this,DashBoard.class));
    }
}
