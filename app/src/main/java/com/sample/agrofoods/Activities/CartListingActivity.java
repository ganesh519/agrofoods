package com.sample.agrofoods.Activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.HashMap;

public class CartListingActivity extends AppCompatActivity {
    PrefManager prefManager;
    String token;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        prefManager = new PrefManager(CartListingActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        Toast.makeText(CartListingActivity.this, token, Toast.LENGTH_SHORT).show();
    }
}
