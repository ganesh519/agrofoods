package com.sample.agrofoods.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;




import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.AddAddressResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddAddressActivity extends AppCompatActivity {
    Geocoder geocoder;
    String latitude1, longitude1;
    List<Address> addresses;
    double latitute_tt, longitiu_II;

    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.name_til)
    TextInputLayout nameTil;
    @BindView(R.id.etAddressline1)
    EditText etAddressline1;
    @BindView(R.id.adreess1_til)
    TextInputLayout adreess1Til;
    @BindView(R.id.etAddressline2)
    EditText etAddressline2;
    @BindView(R.id.adreess2_til)
    TextInputLayout adreess2Til;
    @BindView(R.id.etArea)
    EditText etArea;
    @BindView(R.id.area_til)
    TextInputLayout areaTil;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.city_till)
    TextInputLayout cityTill;
    @BindView(R.id.etState)
    EditText etState;
    @BindView(R.id.state_til)
    TextInputLayout stateTil;
    @BindView(R.id.etCountry)
    EditText etCountry;
    @BindView(R.id.country_til)
    TextInputLayout countryTil;
    @BindView(R.id.etPincode)
    EditText etPincode;
    @BindView(R.id.zip_til)
    TextInputLayout zipTil;
    @BindView(R.id.etPhoneNo)
    EditText etPhoneNo;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etAlternateno)
    EditText etAlternateno;
    @BindView(R.id.ti_etAlternateno)
    TextInputLayout tiEtAlternateno;
    @BindView(R.id.checkboxDefault)
    CheckBox checkboxDefault;
    @BindView(R.id.btnApply)
    Button btnApply;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;

    private static final String TAG = "AddAddressActivity";
    AppController appController;


    private PrefManager pref;
    String token, checkId, loc_area, loc_pincode, shippingcaharge, tokenValue, deviceId, username, mobile;
    boolean checkoutStatus, locationPrefBoolean;

    // private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Address");


        geocoder = new Geocoder(this, Locale.getDefault());

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        token = profile.get("Token");


        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }

        appController = (AppController) getApplication();

        etName.setText(username);
        etPhoneNo.setText(mobile);
        etArea.setText(loc_area);
        etPincode.setText(loc_pincode);
        etCity.setText("Hyderabad");
        etState.setText("Telangana");
        etCountry.setText("India");

        etName.addTextChangedListener(new MyTextWatcher(etName));
        etAddressline1.addTextChangedListener(new MyTextWatcher(etAddressline1));
        etAddressline2.addTextChangedListener(new MyTextWatcher(etAddressline2));
        etArea.addTextChangedListener(new MyTextWatcher(etArea));
        etCity.addTextChangedListener(new MyTextWatcher(etCity));
        etState.addTextChangedListener(new MyTextWatcher(etState));
        etCountry.addTextChangedListener(new MyTextWatcher(etCountry));
        etPincode.addTextChangedListener(new MyTextWatcher(etPincode));
        etPhoneNo.addTextChangedListener(new MyTextWatcher(etPhoneNo));
      //  etAlternateno.addTextChangedListener(new MyTextWatcher(etAlternateno));


        checkId = "1";
        checkboxDefault.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    checkId = "1";
                } else {

                    checkId = "0";
                }
            }
        });


    }


    @OnClick(R.id.btnApply)
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.btnApply:

                boolean isConnected = appController.isConnection();
                if (isConnected) {
                    validateForm();
                } else {

                    String message = "No Internet Connection!..";
                    int color = Color.RED;
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }
                break;
        }


    }


    //validate inputs...
    private void validateForm() {

        String name = etName.getText().toString();
        String lname = etName.getText().toString();
        String addressline1 = etAddressline1.getText().toString();
        String addressline2 = etAddressline2.getText().toString();
        String area = etArea.getText().toString();
        String city = etCity.getText().toString();
        String state = etState.getText().toString();
        String country = etCountry.getText().toString();
        String pincode = etPincode.getText().toString();
        String phone = etPhoneNo.getText().toString();
        String alternateno = etAlternateno.getText().toString();


        if ((!isValidName(name))) {
            return;
        }
        if (!isValidPinCode(pincode)) {
            return;
        }
        if ((!isValidPhoneNumber(phone))) {
            return;
        }
        /*if ((!isValidPhoneNumber1(alternateno))) {
            return;
        }
*/

     //   btnApply.setEnabled(false);
       // btnApply.setVisibility(View.GONE);

        ProgressDialog progressDialog = new ProgressDialog(AddAddressActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<AddAddressResponse> call = RetrofitClient.getInstance().getApi().AddAddress(token, name, "", "", phone, pincode, addressline1, addressline2, city, area, country, state, alternateno, checkId);
        call.enqueue(new Callback<AddAddressResponse>() {
            @Override
            public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                if (response.isSuccessful()) ;
                AddAddressResponse addAddressResponse = response.body();
                if (addAddressResponse.getCode() == 200) {
                    progressDialog.dismiss();
                    Intent intent = new Intent(AddAddressActivity.this, AddressListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("Checkout", checkoutStatus);
                    startActivity(intent);
                }
                else if (addAddressResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(AddAddressActivity.this, addAddressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AddAddressActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();
             //   btnApply.setEnabled(true);
             //   btnApply.setVisibility(View.VISIBLE);

            }
        });

    }

    // validate name
    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            nameTil.setError("name is required");
            requestFocus(etName);
            return false;
        } else if (!matcher.matches()) {
            nameTil.setError("Enter Alphabets Only");
            requestFocus(etName);
            return false;
        } else if (name.length() < 5 || name.length() > 20) {
            nameTil.setError("Name Should be 5 to 20 characters");
            requestFocus(etName);
            return false;
        } else {
            nameTil.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhoneNo);
            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhoneNo);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    private boolean isValidPhoneNumber1(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            tiEtAlternateno.setError("Phone no is required");
            requestFocus(etAlternateno);
            return false;
        } else if (!matcher.matches()) {
            tiEtAlternateno.setError("Enter a valid mobile");
            requestFocus(etAlternateno);
            return false;
        } else {
            tiEtAlternateno.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    // valid OTP
    private boolean isValidPinCode(String pincode) {


        if (pincode.isEmpty()) {
            zipTil.setError("Pincode is required");
            requestFocus(etPincode);
            etCity.setText("");
            etState.setText("");
            return false;
        } else if (pincode.length() < 6) {
            zipTil.setError("Enter a valid Pincode");

            return false;
        } else {
            zipTil.setErrorEnabled(false);
        }

        return true;
    }


    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etName:
                    isValidName(etName.getText().toString().trim());
                    break;
                case R.id.etPhoneNo:
                    isValidPhoneNumber(etPhoneNo.getText().toString().trim());
                    break;
                case R.id.etPincode:
                    isValidPinCode(etPincode.getText().toString().trim());
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}

