package com.sample.agrofoods.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import com.sample.agrofoods.Adapters.AddressAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.AddressListResponse;
import com.sample.agrofoods.Models.AddressModelItem;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressListActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    LocationManager locationManager;

    @BindView(R.id.btnAddAddress)
    Button btnAddAddress;
    @BindView(R.id.addressList)
    ListView addressList;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.txtAlert)
    TextView txtAlert;

    private PrefManager pref;
    private AddressAdapter cartAdapter;

    String userId, itemslength, module, customerId, totalPrice, tokenValue, deviceId;
    int checked;
    boolean checkoutStatus;
    ArrayList<AddressModelItem> addressModelItems = new ArrayList<>();
    private String provider = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Address");
        AppController app = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("Token");

        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }
        if (app.isConnection()) {
            prepareAddressData();

        }
        else {
            Toast.makeText(this, "No Internet Connection!..", Toast.LENGTH_SHORT).show();

        }
    }

    private void prepareAddressData() {
        ProgressDialog progressDialog=new ProgressDialog(AddressListActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<AddressListResponse> call= RetrofitClient.getInstance().getApi().AddressList(userId);
        call.enqueue(new Callback<AddressListResponse>() {
            @Override
            public void onResponse(Call<AddressListResponse> call, Response<AddressListResponse> response) {
                if (response.isSuccessful());
                AddressListResponse addressListResponse=response.body();
                if (addressListResponse.getCode() == 200){
                    progressDialog.dismiss();
                    for (AddressListResponse.DataBean address : addressListResponse.getData()) {

                        addressModelItems.add(new AddressModelItem(String.valueOf(address.getId()), address.getFirst_name(), address.getAddress_line1(), address.getAddress_line2(), address.getArea(), address.getCity(),
                                address.getState(), address.getPin_code(),"", address.getPhone(), address.getAlternate_phone(),
                                String.valueOf(address.getDefaults())));
                    }

                    cartAdapter = new AddressAdapter(AddressListActivity.this, addressModelItems, checkoutStatus);
                    addressList.setAdapter(cartAdapter);
                    cartAdapter.setSelectedIndex(checked);
                    addressList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            cartAdapter.setSelectedIndex(position);
                            cartAdapter.notifyDataSetChanged();
                        }
                    });
                }

                else if (addressListResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(AddressListActivity.this, addressListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (addressListResponse.getCode() == 204){
                    progressDialog.dismiss();
                    Toast.makeText(AddressListActivity.this, addressListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddressListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AddressListActivity.this, "Server Error..", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @OnClick(R.id.btnAddAddress)
    public void onViewClicked() {
        Intent intent = new Intent(AddressListActivity.this, AddAddressActivity.class);
        intent.putExtra("Checkout", checkoutStatus);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                if (checkoutStatus) {
                    Intent intent = new Intent(AddressListActivity.this, CheckoutActivity.class);
                    intent.putExtra("Checkout", checkoutStatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
                    intent.putExtra("Checkout", checkoutStatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (checkoutStatus) {
            Intent intent = new Intent(AddressListActivity.this, CheckoutActivity.class);
            intent.putExtra("Checkout", checkoutStatus);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
            intent.putExtra("Checkout", checkoutStatus);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
