package com.sample.agrofoods.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.sample.agrofoods.Adapters.PaymentTpyeRecyclerAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.CheckoutResponse;
import com.sample.agrofoods.Models.OrderPlaceResponse;
import com.sample.agrofoods.PaymentTypeInterface;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity implements PaymentTypeInterface {
    PaymentTypeInterface paymentTypeInterface;

    public static final String TAG = "CheckoutActivity : ";
    @BindView(R.id.txtChangeAddress)
    TextView txtChangeAddress;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.textView9)
    TextView textView9;
    @BindView(R.id.txtItems)
    TextView txtItems;
    @BindView(R.id.textView12)
    TextView textView12;
    @BindView(R.id.txtSubTotal)
    TextView txtSubTotal;
    @BindView(R.id.textView15)
    TextView textView15;
    @BindView(R.id.txtDiscount)
    TextView txtDiscount;
    @BindView(R.id.textView17)
    TextView textView17;
    @BindView(R.id.txtGrandTotal)
    TextView txtGrandTotal;
    @BindView(R.id.txt145)
    TextView txt145;
    @BindView(R.id.txtDeliver)
    TextView txtDeliver;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.textView21)
    TextView textView21;
    @BindView(R.id.txtAmountTobepaid)
    TextView txtAmountTobepaid;
    @BindView(R.id.txtPaymentMode)
    TextView txtPaymentMode;
    @BindView(R.id.bottamlayout)
    RelativeLayout bottamlayout;

    @BindView(R.id.btnOrder)
    Button btnOrder;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    @BindView(R.id.recycler_paymentoptions)
    RecyclerView recyclerPaymentoptions;

    private double price;
    AppController appController;
    @BindView(R.id.txtShippingAddress)
    TextView txtShippingAddress;
    @BindView(R.id.txtOrderDetails)
    TextView txtOrderDetails;
    @BindView(R.id.orderRelative)
    RelativeLayout orderRelative;

    PaymentTpyeRecyclerAdapter paymentTpyeRecyclerAdapter;
    /*@BindView(R.id.rbCod)
    RadioButton rbCod;
    @BindView(R.id.rbPayu)
    RadioButton rbPayu;*/

    private ProgressDialog pDialog;
    private PrefManager pref;
    String token, addressid, payment_name, email, tokenValue, deviceId, loc_area, loc_pincode, shippingCharges;
    boolean cartStatus;

    String txnId;
    private AppCompatActivity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");

        appController = (AppController) getApplication();
        paymentTypeInterface = (PaymentTypeInterface) this;
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        token = profile.get("Token");
        addressid="";
        // transaction id
        txnId = System.currentTimeMillis() + "";

        if (getIntent().getExtras() != null) {
            addressid = getIntent().getStringExtra("addressId");
            cartStatus = getIntent().getBooleanExtra("Checkout", false);

        }



        if (appController.isConnection()) {
           // showLocations();
            checkoutData();
            init();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), DashBoard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                   // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }

    }

    private void init() {
        activity = CheckoutActivity.this;

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PaymentData();

            }
        });

        txtChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckoutActivity.this, AddressListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout", cartStatus);
                activity.startActivity(intent);
            //    activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    // checkout response
    private void checkoutData() {
        ProgressDialog progressDialog=new ProgressDialog(CheckoutActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Call<CheckoutResponse> call= RetrofitClient.getInstance().getApi().CheckoutData(token,addressid);
        call.enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(Call<CheckoutResponse> call, Response<CheckoutResponse> response) {
                if (response.isSuccessful());
                CheckoutResponse checkoutResponse=response.body();
                if (checkoutResponse.getCode() == 200){
                    progressDialog.dismiss();
                    CheckoutResponse.DataBean dataBean=checkoutResponse.getData();
                    txtSubTotal.setText("\u20B9" + String.valueOf(dataBean.getSub_total()));
                   // txtDiscount.setText("\u20B9" + cartResponse.getData().getFinal_discount());
                   // txtGrandTotal.setText("\u20B9" + String.valueOf(dataBean.getGrand_total()));
                    txtAmountTobepaid.setText("\u20B9" + String.valueOf(dataBean.getGrand_total()));

                    if (checkoutResponse.getData().getAddres().size() != 0) {
                        addressid = String.valueOf(checkoutResponse.getData().getAddres().get(0).getAddress_id());

                        // username and address
                        txtUserName.setText(checkoutResponse.getData().getAddres().get(0).getName());
                        txtAddress.setText(checkoutResponse.getData().getAddres().get(0).getAddress_line1() + ","
                                + checkoutResponse.getData().getAddres().get(0).getAddress_line2() + ","
                                + checkoutResponse.getData().getAddres().get(0).getArea() + ","
                                + checkoutResponse.getData().getAddres().get(0).getCity() + "," +
                                checkoutResponse.getData().getAddres().get(0).getState() + ","
                                + checkoutResponse.getData().getAddres().get(0).getPincode());
                    } else {
                        txtAddress.setText("Please Add Address !! ");
                        txtAddress.setTextColor(Color.RED);
                        txtAddress.setTextSize(20);
                    }

                    List<CheckoutResponse.DataBean.PaymentMethodsBean> paymentMethodsBeanList=dataBean.getPayment_methods();
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    recyclerPaymentoptions.setLayoutManager(layoutManager);
                    recyclerPaymentoptions.setItemAnimator(new DefaultItemAnimator());

                    paymentTpyeRecyclerAdapter = new PaymentTpyeRecyclerAdapter(CheckoutActivity.this, paymentMethodsBeanList, paymentTypeInterface);
                    recyclerPaymentoptions.setAdapter(paymentTpyeRecyclerAdapter);


                }

                else if (checkoutResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(CheckoutActivity.this, checkoutResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (checkoutResponse.getCode() == 204){
                    progressDialog.dismiss();
                    Toast.makeText(CheckoutActivity.this, checkoutResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckoutResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CheckoutActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onItemClick(List<CheckoutResponse.DataBean.PaymentMethodsBean> paymentGatewayBean, int position) {
        payment_name = paymentGatewayBean.get(position).getCod();
        // Toast.makeText(getApplicationContext(),""+paymentGatewayBean.get(position).getId(),Toast.LENGTH_SHORT).show();
    }


    private void PaymentData() {

        if (addressid == null) {
           // Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Choose Address" + "</font>"), Snackbar.LENGTH_SHORT).show();

            Toast.makeText(CheckoutActivity.this, "Please Choose Address ", Toast.LENGTH_SHORT).show();
            return;
        }

        if (payment_name == null) {
            //Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Select Payment method" + "</font>"), Snackbar.LENGTH_SHORT).show();

            Toast.makeText(CheckoutActivity.this, "Please Select Payment method ", Toast.LENGTH_SHORT).show();
            return;
        }


        postOrder(addressid, payment_name);

    }


    // order post
    private void postOrder(final String addressid, String payment_name) {

        ProgressDialog progressDialog=new ProgressDialog(CheckoutActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<OrderPlaceResponse> call=RetrofitClient.getInstance().getApi().OrderPlace(token,addressid,payment_name);
        call.enqueue(new Callback<OrderPlaceResponse>() {
            @Override
            public void onResponse(Call<OrderPlaceResponse> call, Response<OrderPlaceResponse> response) {
                if (response.isSuccessful());
                OrderPlaceResponse orderPlaceResponse=response.body();
                if (orderPlaceResponse.getCode() == 200){
                    progressDialog.dismiss();
                    showCustomDialog(orderPlaceResponse.getOrder_id(), orderPlaceResponse.getMessage());

                    // cart update
                    //appController.cartCount(token);
                }
                else if (orderPlaceResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(CheckoutActivity.this, orderPlaceResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OrderPlaceResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CheckoutActivity.this, "Server Error..", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void showCustomDialog(String order_id, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        TextView txtmessage = (TextView) dialogView.findViewById(R.id.txtMessage);
        txtmessage.setText("Order Placed Sucessfully");

        Button buttonOk = (Button) dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent order_detail = new Intent(CheckoutActivity.this, OrdersListActivity.class);
                order_detail.putExtra("Order_ID", order_id);
                order_detail.putExtra("Checkout", cartStatus);
                order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(order_detail);
                finish();
            }
        });

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
      //  hidePDialog();
    }

//    private void hidePDialog() {
//        if (pDialog != null) {
//            pDialog.dismiss();
//            pDialog = null;
//        }
//    }

    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
