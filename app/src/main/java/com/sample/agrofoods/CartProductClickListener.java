package com.sample.agrofoods;


import com.sample.agrofoods.Models.Product;

public interface CartProductClickListener {

    void onMinusClick(Product product);

    void onPlusClick(Product product);

    void onSaveClick(Product product);

    void onRemoveDialog(Product product);


}
