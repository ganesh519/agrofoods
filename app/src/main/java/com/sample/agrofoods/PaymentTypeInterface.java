package com.sample.agrofoods;



import com.sample.agrofoods.Models.CheckoutResponse;

import java.util.List;

public interface PaymentTypeInterface {

    void onItemClick(List<CheckoutResponse.DataBean.PaymentMethodsBean> paymentGatewayBean, int position);
}
