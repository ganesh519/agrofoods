package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Activities.ProductDetailsActivity;
import com.sample.agrofoods.Models.ProductSearchResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.sample.agrofoods.Utilities.capitalize;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder>{
    private Context mContext;
    List<ProductSearchResponse.DataBean> homeList;

    public SearchAdapter(Context applicationContext, List<ProductSearchResponse.DataBean> docsBeanList) {
        this.mContext=applicationContext;
        this.homeList=docsBeanList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView txtPrice, txtName, txtDiscountPrice, txtDiscountTag,product_unit;
        //  public RatingBar ratingBar;
        public LinearLayout tagLayout;
        public CardView linearLayout;

        public MyViewHolder(View view) {
            super(view);

            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
           // txtPrice = (TextView) view.findViewById(R.id.txtproductPrice);
            product_unit = (TextView) view.findViewById(R.id.product_unit);
            txtName = (TextView) view.findViewById(R.id.txtproductName);
          //  txtDiscountPrice = (TextView) view.findViewById(R.id.txtDiscountPrice);
            linearLayout=(CardView)view.findViewById(R.id.parentLayout);
            tagLayout = (LinearLayout)view.findViewById(R.id.tagLayout);
            txtDiscountTag = (TextView)view.findViewById(R.id.txtDiscountTag);


        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.products_item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
       // final ProductResponse.DataBean home = homeList.get(position);

        Picasso.get().load(homeList.get(position).getImage()).error(R.drawable.placeholder).into(holder.thumbnail);
        holder.txtName.setText(capitalize(homeList.get(position).getName()));
       // holder.txtDiscountPrice.setText("\u20B9"+home.getSelling_price());

       // holder.txtPrice.setText("\u20B9" + home.getMrp_price());
       // holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
      //  holder.txtPrice.setTextColor(Color.RED);

      //  holder.product_unit.setText("" +home.getUnit_value()+home.getUnit_name());

        //  holder.ratingBar.setRating(Float.parseFloat(home.getRating()));
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId",String.valueOf(homeList.get(position).getProduct_id()));
                intent.putExtra("productName",homeList.get(position).getName());
                intent.putExtra("status","search");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                // activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }});


    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }



}
