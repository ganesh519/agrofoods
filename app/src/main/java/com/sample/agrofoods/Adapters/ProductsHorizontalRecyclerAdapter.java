package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.sample.agrofoods.Activities.ProductDetailsActivity;
import com.sample.agrofoods.Models.HomeDataResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductsHorizontalRecyclerAdapter extends RecyclerView.Adapter<ProductsHorizontalRecyclerAdapter.Holder> {
    Context context;
    List<HomeDataResponse.DataBean.ProductsBean> productsResponses;
    public ProductsHorizontalRecyclerAdapter(Context context, List<HomeDataResponse.DataBean.ProductsBean> productsResponses) {
        this.context=context;
        this.productsResponses=productsResponses;
    }

    @NonNull
    @Override
    public ProductsHorizontalRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_horizontal, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsHorizontalRecyclerAdapter.Holder holder, int position) {
      //  holder.product_price.setText(productsResponses[position].getP_price());
        holder.txtproductName.setText(productsResponses.get(position).getName());
        Picasso.get().load(productsResponses.get(position).getImage()).into(holder.thumbnail);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pid=String.valueOf(productsResponses.get(position).getId());
                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("productId",pid);
                intent.putExtra("productName",productsResponses.get(position).getName());
                intent.putExtra("status","home");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return productsResponses.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        ImageView thumbnail;
        TextView txtproductName,product_price;

        public Holder(@NonNull View itemView) {
            super(itemView);
            thumbnail=itemView.findViewById(R.id.thumbnail);
            txtproductName=itemView.findViewById(R.id.txtproductName);
          //  product_price=itemView.findViewById(R.id.product_price);

        }
    }
}
