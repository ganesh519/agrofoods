package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Activities.ProductListActivity;
import com.sample.agrofoods.Activities.SubSubCatActivity;
import com.sample.agrofoods.Models.AllProductsResponse;
import com.sample.agrofoods.Models.SubCategoriesResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SubCategoriesListRecyclerAdapter extends RecyclerView.Adapter<SubCategoriesListRecyclerAdapter.Holder> {
    List<SubCategoriesResponse.DataBean> receivePaymentModels;
    Context context;
    String catId;
    List<AllProductsResponse.DataBean> productsBeanList;
    public SubCategoriesListRecyclerAdapter(List<SubCategoriesResponse.DataBean> receivePaymentModels, Context context, String catId, List<AllProductsResponse.DataBean> productsBeanList) {
        this.context=context;
        this.receivePaymentModels=receivePaymentModels;
        this.catId=catId;
        this.productsBeanList=productsBeanList;
    }

    @NonNull
    @Override
    public SubCategoriesListRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoriesListRecyclerAdapter.Holder holder, int position) {

        holder.itemTitle.setText(receivePaymentModels.get(position).getName());
        Picasso.get().load(receivePaymentModels.get(position).getImage()).error(R.drawable.placeholder).into(holder.product_image);

//        final HorizonalProductsRecyclerAdapter adapter = new HorizonalProductsRecyclerAdapter(context, productsBeanList);
//        holder.recycler_view_list.setHasFixedSize(true);
//        holder.recycler_view_list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
//        holder.recycler_view_list.setNestedScrollingEnabled(false);
//        holder.recycler_view_list.setAdapter(adapter);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id=String.valueOf(receivePaymentModels.get(position).getId());
                if (receivePaymentModels.get(position).getSub_sub_categorie() == 1){
                    Intent intent = new Intent(context, SubSubCatActivity.class);
                    intent.putExtra("catId", id);
                    intent.putExtra("title", receivePaymentModels.get(position).getName());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
                else {
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra("catId", id);
                    intent.putExtra("title", receivePaymentModels.get(position).getName());
                    intent.putExtra("subcatId",id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return receivePaymentModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
      //  CircleImageView thumbnail;
        TextView itemTitle;
        Button btnMore;
        RecyclerView recycler_view_list;
        ImageView img_forward,product_image;

        public Holder(@NonNull View itemView) {
            super(itemView);
            itemTitle=itemView.findViewById(R.id.itemTitle);
            btnMore=itemView.findViewById(R.id.btnMore);
            img_forward=itemView.findViewById(R.id.img_forward);
            product_image=itemView.findViewById(R.id.product_image);
            recycler_view_list=itemView.findViewById(R.id.recycler_view_list);
         //   thumbnail=itemView.findViewById(R.id.thumbnail);

        }
    }

}
