package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.CartProductClickListener;
import com.sample.agrofoods.Models.Product;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.Holder> {
   // List<CartListingResponse.DataBean.CartBean> receivePaymentModels;
    Context context;
    SweetAlertDialog sd;
    private boolean checkInternet;
    String cart_id, user_id, pid;
    private CartProductClickListener productClickListener;
    private List<Product> productsModelList;
    public CartRecyclerAdapter(Context context,  List<Product> productsModelList, CartProductClickListener cartProductClickListener) {
        this.context=context;
        this.productsModelList=productsModelList;
        this.productClickListener=cartProductClickListener;
    }

    @NonNull
    @Override
    public CartRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_product, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartRecyclerAdapter.Holder holder, int position) {
        final Product productsModel = productsModelList.get(position);
        holder.product_name.setText(productsModel.getProduct_name());
        holder.product_unit.setText(productsModel.getQuantity());
        holder.final_product_Price.setText(productsModel.getDiscount_price());
        holder.product_Price.setText(productsModel.getMrp_price());
        holder.product_Price.setPaintFlags(holder.product_Price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.product_Price.setTextColor(Color.RED);
        holder.product_quantity.setText(String.valueOf(productsModel.getPurchase_quantity()));

//        int save=Integer.parseInt(productsModel.getMrp_price())-Integer.parseInt(productsModel.getDiscount_price());
        holder.txt_save.setText("save :"+productsModel.getProfit());

        Picasso.get().load(productsModel.getImages()).into(holder.thumbnail1);

        holder.product_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                productClickListener.onPlusClick(productsModel);
            }
        });

        holder.product_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productClickListener.onMinusClick(productsModel);
            }
        });


        holder.img_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productClickListener.onRemoveDialog(productsModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productsModelList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        ImageView thumbnail1,img_remove;
        TextView product_name,product_unit,product_Price,final_product_Price,product_quantity,product_plus,product_minus,txt_save;
        ProgressBar progressbar_addincrease;
        public Holder(@NonNull View itemView) {
            super(itemView);
            product_name=itemView.findViewById(R.id.product_name);
            product_unit=itemView.findViewById(R.id.product_unit);
            product_Price=itemView.findViewById(R.id.product_Price);
            final_product_Price=itemView.findViewById(R.id.final_product_Price);
            product_quantity=itemView.findViewById(R.id.product_quantity);
            product_plus=itemView.findViewById(R.id.product_plus);
            product_minus=itemView.findViewById(R.id.product_minus);
            txt_save=itemView.findViewById(R.id.txt_save);

            progressbar_addincrease=itemView.findViewById(R.id.progressbar_addincrease);


            thumbnail1=itemView.findViewById(R.id.thumbnail1);
            img_remove=itemView.findViewById(R.id.img_remove);

        }
    }


    private void getData(String cart_id) {
    }

}
