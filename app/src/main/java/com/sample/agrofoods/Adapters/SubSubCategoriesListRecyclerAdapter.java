package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Activities.ProductListActivity;
import com.sample.agrofoods.Models.SubCategoriesResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SubSubCategoriesListRecyclerAdapter extends RecyclerView.Adapter<SubSubCategoriesListRecyclerAdapter.Holder> {
    List<SubCategoriesResponse.DataBean> receivePaymentModels;
    Context context;
    String catId;

    public SubSubCategoriesListRecyclerAdapter(List<SubCategoriesResponse.DataBean> receivePaymentModels, Context context, String catId) {
        this.context=context;
        this.receivePaymentModels=receivePaymentModels;
        this.catId=catId;

    }

    @NonNull
    @Override
    public SubSubCategoriesListRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_sub_categories, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubSubCategoriesListRecyclerAdapter.Holder holder, int position) {

        holder.itemTitle.setText(receivePaymentModels.get(position).getName());

        Picasso.get().load(receivePaymentModels.get(position).getImage()).error(R.drawable.placeholder).into(holder.product_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id=String.valueOf(receivePaymentModels.get(position).getId());
                Intent intent = new Intent(context, ProductListActivity.class);
                intent.putExtra("catId", id);
                intent.putExtra("title", receivePaymentModels.get(position).getName());
                intent.putExtra("subcatId",id);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return receivePaymentModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
      //  CircleImageView thumbnail;
        TextView itemTitle;
        Button btnMore;
        RecyclerView recycler_view_list;
        ImageView product_image,img_forward;

        public Holder(@NonNull View itemView) {
            super(itemView);
            itemTitle=itemView.findViewById(R.id.itemTitle);
            btnMore=itemView.findViewById(R.id.btnMore);
            product_image=itemView.findViewById(R.id.product_image);
            img_forward=itemView.findViewById(R.id.img_forward);
            recycler_view_list=itemView.findViewById(R.id.recycler_view_list);
         //   thumbnail=itemView.findViewById(R.id.thumbnail);

        }
    }

}
