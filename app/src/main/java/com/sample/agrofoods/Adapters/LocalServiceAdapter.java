package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Activities.LocalServicesActivity;
import com.sample.agrofoods.CartProductClickListener;
import com.sample.agrofoods.Models.LocalServiceResponse;
import com.sample.agrofoods.Models.Product;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LocalServiceAdapter extends RecyclerView.Adapter<LocalServiceAdapter.Holder> {
    Context context;
    LocalServiceResponse[] localServiceResponse;
    public LocalServiceAdapter(Context context, LocalServiceResponse[] localServiceResponse) {
        this.context=context;
        this.localServiceResponse=localServiceResponse;

    }


    @NonNull
    @Override
    public LocalServiceAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.localservices, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LocalServiceAdapter.Holder holder, int position) {

        Picasso.get().load(localServiceResponse[position].getImage()).into(holder.thumbnail1);

        holder.txt_service.setText(localServiceResponse[position].getService());
        holder.txt_name.setText(localServiceResponse[position].getName());
        holder.txt_mno.setText(localServiceResponse[position].getMobileno());
        holder.txt_amno.setText(localServiceResponse[position].getAltermno());
        holder.txt_desc.setText(localServiceResponse[position].getDesc());


    }

    @Override
    public int getItemCount() {
        return localServiceResponse.length;
    }

    public class Holder extends RecyclerView.ViewHolder{
        ImageView thumbnail1;
        TextView txt_service,txt_name,txt_mno,txt_amno,txt_desc;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_service=itemView.findViewById(R.id.txt_service);
            txt_desc=itemView.findViewById(R.id.txt_desc);
            txt_amno=itemView.findViewById(R.id.txt_amno);
            txt_mno=itemView.findViewById(R.id.txt_mno);
            txt_name=itemView.findViewById(R.id.txt_name);
            thumbnail1=itemView.findViewById(R.id.thumbnail1);




        }
    }

}
