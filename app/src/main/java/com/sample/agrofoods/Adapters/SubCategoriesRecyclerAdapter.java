package com.sample.agrofoods.Adapters;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Activities.ProductListActivity;
import com.sample.agrofoods.Activities.SubSubCatActivity;
import com.sample.agrofoods.Models.SubCategoriesResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SubCategoriesRecyclerAdapter extends RecyclerView.Adapter<SubCategoriesRecyclerAdapter.Holder> {
    List<SubCategoriesResponse.DataBean> receivePaymentModels;
    Context context;
    public SubCategoriesRecyclerAdapter(List<SubCategoriesResponse.DataBean> receivePaymentModels, Context context) {
        this.context=context;
        this.receivePaymentModels=receivePaymentModels;
    }

    @NonNull
    @Override
    public SubCategoriesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcat_card, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoriesRecyclerAdapter.Holder holder, int position) {

        holder.txtCatName.setText(receivePaymentModels.get(position).getName());
        Picasso.get().load(receivePaymentModels.get(position).getImage()).error(R.drawable.placeholder).into(holder.thumbnail);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id=String.valueOf(receivePaymentModels.get(position).getId());
                if (receivePaymentModels.get(position).getSub_sub_categorie() == 1){
                    Intent intent = new Intent(context, SubSubCatActivity.class);
                    intent.putExtra("catId", id);
                    intent.putExtra("title", receivePaymentModels.get(position).getName());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }
                else {
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra("catId", id);
                    intent.putExtra("title", receivePaymentModels.get(position).getName());
                    intent.putExtra("subcatId",id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return receivePaymentModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        CircleImageView thumbnail;
        TextView txtCatName;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txtCatName=itemView.findViewById(R.id.txtCatName);
            thumbnail=itemView.findViewById(R.id.thumbnail);

        }
    }
}
