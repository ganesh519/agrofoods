package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.sample.agrofoods.Models.SpinnerModel;
import com.sample.agrofoods.R;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {
    List<SpinnerModel> models;
    Context context;


    public SpinnerAdapter(List<SpinnerModel> customerTypesListBeanList, Context context) {
        this.models=customerTypesListBeanList;
        this.context=context;

    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = View.inflate(context, R.layout.spinner_row_sample, null);
        }

       // Typeface regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));

        TextView txtName = (TextView) convertView.findViewById(R.id.serList);

        txtName.setText(models.get(position).getName());
//        String first=models.get(position).getName();
//        String chars = capitalize(first);
//        txtName.setText(chars);
//        txtName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);


        // txtName.setTypeface(regular);


        return convertView;
    }
}