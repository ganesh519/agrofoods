package com.sample.agrofoods.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;

import com.sample.agrofoods.Activities.GroceryHomeActivity1;
import com.sample.agrofoods.Activities.ProductListActivity;
import com.sample.agrofoods.Models.AllCategoriesResponse;
import com.sample.agrofoods.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllCategoriesRecyclerAdapter extends RecyclerView.Adapter<AllCategoriesRecyclerAdapter.Holder> {
    List<AllCategoriesResponse.DataBean> receivePaymentModels;
    Context context;
    public AllCategoriesRecyclerAdapter(Context context, List<AllCategoriesResponse.DataBean> receivePaymentModels) {
        this.context=context;
        this.receivePaymentModels=receivePaymentModels;
    }

    @NonNull
    @Override
    public AllCategoriesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_card, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AllCategoriesRecyclerAdapter.Holder holder, int position) {

        setFadeAnimation(holder.itemView);

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();

      //  holder.cardView.setCardBackgroundColor(color1);

        holder.txt_name.setText(receivePaymentModels.get(position).getName());
        Picasso.get().load(receivePaymentModels.get(position).getImage()).error(R.drawable.placeholder).into(holder.image_category);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (receivePaymentModels.get(position).getSub_categorie() == 1){
                    Intent intent = new Intent(context, GroceryHomeActivity1.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("CatId", String.valueOf(receivePaymentModels.get(position).getId()));
                    intent.putExtra("Cat_name",receivePaymentModels.get(position).getName());
                    context.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra("catId", String.valueOf(receivePaymentModels.get(position).getId()));
                    intent.putExtra("title", receivePaymentModels.get(position).getName());
                    intent.putExtra("subcatId","");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return receivePaymentModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        ImageView image_category;
        TextView txt_name;
       // CardV

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txtTitle);
            image_category=itemView.findViewById(R.id.thumbnail);

        }
    }

    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}
