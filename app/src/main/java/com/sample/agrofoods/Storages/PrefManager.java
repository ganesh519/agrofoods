package com.sample.agrofoods.Storages;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

/**
 * Created by Ravi on 08/07/15.
 */
public class PrefManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "AgroFoods";

    // All Shared Preferences Keys
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_MOBILE = "mobile";
    private static final String KEY_DEVICEID="deviceId";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void Login(String f_name,String mobile,String token){
        editor.putString(KEY_NAME, f_name);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_TOKEN,token);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }

    public void Device(String deviceid){
        editor.putString(KEY_DEVICEID,deviceid);
        editor.commit();
    }



    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("id", pref.getString(KEY_ID, null));
        profile.put("name", pref.getString(KEY_NAME, null));
        profile.put("Token", pref.getString(KEY_TOKEN, null));
        profile.put("mobile", pref.getString(KEY_MOBILE, null));
        return profile;
    }


}
