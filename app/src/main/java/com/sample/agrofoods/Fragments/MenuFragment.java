package com.sample.agrofoods.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.sample.agrofoods.Activities.LoginActivity;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuFragment extends Fragment {
    RelativeLayout txt_logout;
    PrefManager session;
    @BindView(R.id.relativeprivacy)
    RelativeLayout relativeprivacy;
    @BindView(R.id.relativefaq)
    RelativeLayout relativefaq;
    @BindView(R.id.relative_contactus)
    RelativeLayout relativeContactus;
    @BindView(R.id.txt_logout)
    RelativeLayout txtLogout;
    PrefManager pref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, v);
        pref = new PrefManager(getContext());
        relativeContactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog dialogBuilder = new AlertDialog.Builder(getContext()).create();
                LayoutInflater inflater1 = getLayoutInflater();
                View dialogView = inflater1.inflate(R.layout.contactus, null);
                RelativeLayout relative_cancel = (RelativeLayout) dialogView.findViewById(R.id.relative_cancel);


                relative_cancel.setOnClickListener(view1 -> {
                    // DO SOMETHINGS
                    dialogBuilder.dismiss();
                });

                dialogBuilder.setView(dialogView);
                dialogBuilder.show();
            }
        });
        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog dialogBuilder = new AlertDialog.Builder(getContext()).create();
                LayoutInflater inflater1 = getLayoutInflater();
                View dialogView = inflater1.inflate(R.layout.logout, null);

                RelativeLayout relative_logout = (RelativeLayout) dialogView.findViewById(R.id.relative_logout);
                RelativeLayout relative_cancel = (RelativeLayout) dialogView.findViewById(R.id.relative_cancel);


                relative_cancel.setOnClickListener(view1 -> {
                    // DO SOMETHINGS
                    dialogBuilder.dismiss();
                });

                relative_logout.setOnClickListener(view12 -> {
                    pref.clearSession();
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                });

                dialogBuilder.setView(dialogView);
                dialogBuilder.show();
            }
        });
        return v;
    }
}
