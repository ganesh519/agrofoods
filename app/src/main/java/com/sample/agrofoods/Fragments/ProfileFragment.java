package com.sample.agrofoods.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.sample.agrofoods.Activities.MyOrdersActivity;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.ProfileResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {
    @BindView(R.id.txt_mail)
    TextView txt_mail;
    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.txt_phone)
    TextView txt_phone;
    @BindView(R.id.circle_image)
    CircleImageView circle_image;
    @BindView(R.id.relative_edit)
    RelativeLayout relative_edit;
    @BindView(R.id.relative_myorders)
    RelativeLayout relative_myorders;
    AppController appController;
    PrefManager prefManager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this,v);

       // prefManager = new PrefManager(getContext());
        prefManager = new PrefManager(getContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        String token = profile.get("Token");
        appController = (AppController) getActivity().getApplication();
        boolean isConnected = appController.isConnection();
        if (isConnected) {
            getProfileData(token);
        }
        else {
            Toast.makeText(getActivity(), "No Internet Connection!...", Toast.LENGTH_SHORT).show();
        }
        return v;
    }

    private void getProfileData(String token) {

        ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<ProfileResponse> call= RetrofitClient.getInstance().getApi().Profile(token);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful());
                ProfileResponse profileResponse=response.body();

                if (profileResponse.getCode() == 200){
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ProfileResponse.DataBean dataBean=profileResponse.getData();
                    txt_name.setText(dataBean.getName());
                    txt_mail.setText(dataBean.getEmail());
                    txt_phone.setText(dataBean.getPhone());

                    if (dataBean.getImage() == null){
                        circle_image.setImageResource(R.drawable.placeholder);
                    }
                    else {
                        Picasso.get().load(dataBean.getImage()).error(R.drawable.placeholder).into(circle_image);
                    }

                }
                else if (profileResponse.getCode() == 401){
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), profileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Serer Error....", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @OnClick({R.id.relative_edit,R.id.relative_myorders})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_edit:
//                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
//                startActivity(intent);
                break;
            case R.id.relative_myorders:
                Intent orders = new Intent(getActivity(), MyOrdersActivity.class);
                orders.putExtra("Checkout", false);
                orders.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(orders);
                break;
        }
    }

}
