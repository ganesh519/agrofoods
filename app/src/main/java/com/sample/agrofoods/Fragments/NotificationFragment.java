package com.sample.agrofoods.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.Adapters.NotificationsAdapter;
import com.sample.agrofoods.Apis.RetrofitClient;
import com.sample.agrofoods.AppController;
import com.sample.agrofoods.Models.NotificationsResponse;
import com.sample.agrofoods.R;
import com.sample.agrofoods.Storages.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {

    @BindView(R.id.recycler_notification)
    RecyclerView recyclerNotification;
    AppController appController;
    PrefManager prefManager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this,v);
        prefManager = new PrefManager(getContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        String token = profile.get("Token");

        appController = (AppController) getActivity().getApplication();
        boolean isConnected = appController.isConnection();
        if (isConnected) {
            getData(token);

        }
        else {
            Toast.makeText(getActivity(), "No Internet Connection!...", Toast.LENGTH_SHORT).show();

        }

        return v;
    }

    private void getData(String token) {

        ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<NotificationsResponse> call= RetrofitClient.getInstance().getApi().Notifications(token);
        call.enqueue(new Callback<NotificationsResponse>() {
            @Override
            public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                if (response.isSuccessful());
                NotificationsResponse notificationsResponse=response.body();
                if (notificationsResponse.getCode() == 200){
                    progressDialog.dismiss();
                    List<NotificationsResponse.DataBean> dataBeanList=notificationsResponse.getData();
                    NotificationsAdapter notificationsAdapter = new NotificationsAdapter(getContext(), dataBeanList);
                    LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext());
                    layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerNotification.setLayoutManager(layoutManager1);
                    recyclerNotification.setNestedScrollingEnabled(false);
                    recyclerNotification.setAdapter(notificationsAdapter);
                  //  RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                   // recyclerNotification.setLayoutManager(mLayoutManager1);
                 //   recyclerNotification.setItemAnimator(new DefaultItemAnimator());
                  //  recyclerNotification.setHasFixedSize(true);


                }
                else if (notificationsResponse.getCode() == 204){
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), notificationsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Server Error...", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
