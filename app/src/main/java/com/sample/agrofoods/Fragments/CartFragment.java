package com.sample.agrofoods.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;




import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.agrofoods.R;

public class CartFragment extends Fragment {
    RecyclerView recycler_cart;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_cart, container, false);
        recycler_cart=v.findViewById(R.id.recycler_cart);
//        ProductsResponse[] productsResponses = new ProductsResponse[]{
//                new ProductsResponse("Old Raw Rice","Rs 50",R.drawable.rice),
//                new ProductsResponse("Toor Dal","Rs 60",R.drawable.dal),
//                new ProductsResponse("Old Raw Rice","Rs 50",R.drawable.rice)
//        };

//        CartRecyclerAdapter productsHorizontalRecyclerAdapter = new CartRecyclerAdapter(getContext(),productsResponses, cartProductClickListener);
//        recycler_cart.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        recycler_cart.setHasFixedSize(true);
//        recycler_cart.setNestedScrollingEnabled(false);
//        recycler_cart.setAdapter(productsHorizontalRecyclerAdapter);
        return v;
    }
}
