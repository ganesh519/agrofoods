package com.sample.agrofoods.Models;

import java.util.List;

public class ProductVariationResponse {


    /**
     * code : 200
     * message : Products Veristions
     * data : [{"id":5,"value":"5Kg","discount":"250.00","price":"270.00"},{"id":6,"value":"10kg","discount":"450.00","price":"490.00"}]
     */

    private int code;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5
         * value : 5Kg
         * discount : 250.00
         * price : 270.00
         */

        private int id;
        private String value;
        private String discount;
        private String price;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
}
