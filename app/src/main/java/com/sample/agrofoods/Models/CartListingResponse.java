package com.sample.agrofoods.Models;

import java.util.List;

public class CartListingResponse {


    /**
     * code : 200
     * message : Cart data
     * data : [{"id":307,"sku":"HMT Rice/హెచ్.ఎం,టి బియ్యం","name":"HMT Rice/హెచ్.ఎం,టి బియ్యం","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/2sjtMpWCzcYiVunF49n1LHdln5xkosxh0qxRZZea.png","qty":"1","stock":12,"price":"1200.00","discount":"1100.00","mrp":"1000","variation_id":357,"options":{"Quantity":"25 Kg"},"item_sub_total":1100,"profit":100,"rowId":"e8e32680-c505-43c3-b9e5-1ebd23768e92"}]
     * subtotal : 1100
     * total : 1100
     */

    private int code;
    private String message;
    private int subtotal;
    private int total;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 307
         * sku : HMT Rice/హెచ్.ఎం,టి బియ్యం
         * name : HMT Rice/హెచ్.ఎం,టి బియ్యం
         * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/2sjtMpWCzcYiVunF49n1LHdln5xkosxh0qxRZZea.png
         * qty : 1
         * stock : 12
         * price : 1200.00
         * discount : 1100.00
         * mrp : 1000
         * variation_id : 357
         * options : {"Quantity":"25 Kg"}
         * item_sub_total : 1100
         * profit : 100
         * rowId : e8e32680-c505-43c3-b9e5-1ebd23768e92
         */

        private int id;
        private String sku;
        private String name;
        private String image;
        private String qty;
        private int stock;
        private String price;
        private String discount;
        private String mrp;
        private int variation_id;
        private OptionsBean options;
        private int item_sub_total;
        private int profit;
        private String rowId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public int getStock() {
            return stock;
        }

        public void setStock(int stock) {
            this.stock = stock;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public int getVariation_id() {
            return variation_id;
        }

        public void setVariation_id(int variation_id) {
            this.variation_id = variation_id;
        }

        public OptionsBean getOptions() {
            return options;
        }

        public void setOptions(OptionsBean options) {
            this.options = options;
        }

        public int getItem_sub_total() {
            return item_sub_total;
        }

        public void setItem_sub_total(int item_sub_total) {
            this.item_sub_total = item_sub_total;
        }

        public int getProfit() {
            return profit;
        }

        public void setProfit(int profit) {
            this.profit = profit;
        }

        public String getRowId() {
            return rowId;
        }

        public void setRowId(String rowId) {
            this.rowId = rowId;
        }

        public static class OptionsBean {
            /**
             * Quantity : 25 Kg
             */

            private String Quantity;

            public String getQuantity() {
                return Quantity;
            }

            public void setQuantity(String Quantity) {
                this.Quantity = Quantity;
            }
        }
    }
}
