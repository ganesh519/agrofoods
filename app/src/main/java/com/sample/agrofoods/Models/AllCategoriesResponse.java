package com.sample.agrofoods.Models;

import java.util.List;

public class AllCategoriesResponse {


    /**
     * code : 200
     * data : [{"id":31,"name":"Books & Stationary","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/AuPEg14ZcsiqlnzggXHANqGi92425hu8rIgjb11f.jpeg","sub_categorie":0,"subcategoreis":[]},{"id":32,"name":"Cleaning And Household","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/rYtURK8WbRsrsbZUstNAbdFjAYq3aXV19jn5mLl4.jpeg","sub_categorie":1,"subcategoreis":[{"id":34,"name":"Detergent Bars"},{"id":69,"name":"Detergent Powder"},{"id":70,"name":"Fabric Care"},{"id":71,"name":"Dishwash Liquids Bars & Scrubbers"},{"id":72,"name":"Floor And toilet Cleaner"},{"id":73,"name":"Brooms"},{"id":74,"name":"Fresheners & Repellents"}]},{"id":36,"name":"Fruits and Veggies","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/IJmGHC78SSOur7gnxwhSjutokqokRZW0QhKHKiOy.jpeg","sub_categorie":0,"subcategoreis":[]},{"id":41,"name":"Other Household & Home accessories","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/8Ki5riuYUIYMc5n983xTR4pjEOXpJFflcjsfDa85.jpeg","sub_categorie":0,"subcategoreis":[]},{"id":42,"name":"Personal Care","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/RJIYm4r7MHxK5Fd6BbT7hu1KaBG1lHqq0uQMbk0W.jpeg","sub_categorie":1,"subcategoreis":[{"id":30,"name":"Bathing Bars & Handwash"},{"id":35,"name":"Feminine Hygiene"},{"id":38,"name":"Hair Care"},{"id":40,"name":"Oral Care"},{"id":45,"name":"Skin Care & Body Care"},{"id":62,"name":"Health & Wellness"}]},{"id":43,"name":"Pooja Needs","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/L0OOxFluELrayesFeUX574RUEplSwXgXVOClgX58.jpeg","sub_categorie":0,"subcategoreis":[]},{"id":46,"name":"Biscuits Namkeen & Snacks","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/fxeqpkeg8MC92htpLrBiYVup9V0mZ02D6DjOkOdH.jpeg","sub_categorie":1,"subcategoreis":[{"id":78,"name":"Noodles /Pasta & Vermicell"}]},{"id":48,"name":"Tea And Beverages","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/h1B2priSSVqWRaYzYIheAHHR14SzPlNoDrnGU09J.jpeg","sub_categorie":1,"subcategoreis":[{"id":75,"name":"Tea Powders"},{"id":76,"name":"Coffee"},{"id":77,"name":"Energy & Sports Drinks"}]},{"id":56,"name":"Rice , Oils Atta, & Foodgrains","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/Z87ZjCrzWpsvWS98BHORW1bBQKK39V9lqsXPAiFg.jpeg","sub_categorie":1,"subcategoreis":[{"id":28,"name":"Atta, Flours & Sooji"},{"id":33,"name":"Dals & Pulses"},{"id":55,"name":"Dry Fruits"},{"id":58,"name":"Salt, Sugar & Jaggery"},{"id":60,"name":"Edible Oils & Ghee"},{"id":61,"name":"Masalas & Spices"}]}]
     */

    private int code;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 31
         * name : Books & Stationary
         * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/AuPEg14ZcsiqlnzggXHANqGi92425hu8rIgjb11f.jpeg
         * sub_categorie : 0
         * subcategoreis : []
         */

        private int id;
        private String name;
        private String image;
        private int sub_categorie;
        private List<?> subcategoreis;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getSub_categorie() {
            return sub_categorie;
        }

        public void setSub_categorie(int sub_categorie) {
            this.sub_categorie = sub_categorie;
        }

        public List<?> getSubcategoreis() {
            return subcategoreis;
        }

        public void setSubcategoreis(List<?> subcategoreis) {
            this.subcategoreis = subcategoreis;
        }
    }
}
