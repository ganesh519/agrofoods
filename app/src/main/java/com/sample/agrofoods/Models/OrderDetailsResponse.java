package com.sample.agrofoods.Models;

import java.util.List;

public class OrderDetailsResponse {

    /**
     * code : 200
     * message : order details
     * data : {"order_id":75,"grand_total":"835.00","item_count":2,"payment_method":"Cash On Delivery","sub_total":"835.00","status":"pending","address":{"id":25,"first_name":"cjxjxxjc","last_name":null,"email":null,"phone":"9234567890","state":null,"city":null,"pin_code":"123456","type":null,"address":null,"created_at":"2020-06-27 11:19:05","updated_at":"2020-06-28 04:09:17","user_id":26,"defaults":0,"address_line1":"chcucuc","address_line2":"cjjcjci","alternate_phone":"9876543210","country":"India","area":"cjcvkvj","deleted_at":null},"order_date":"2020-07-10","expert_date":"2020-07-10","order_items":[{"name":"Soft Noodles","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/uNFxwc0gItVP9bbYu3XxA1njFK7leaEMPVe0CqwX.jpeg","quantity":7,"weight":"600 gm","price":"45.00","item_sub_total":"280.00"},{"name":"Bambino Vermicelli","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/lAeTlu3qv61ab0GQgkfrkxmobu7YzFlcMkdVcFms.png","quantity":15,"weight":"400 gm","price":"40.00","item_sub_total":"555.00"}]}
     */

    private int code;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * order_id : 75
         * grand_total : 835.00
         * item_count : 2
         * payment_method : Cash On Delivery
         * sub_total : 835.00
         * status : pending
         * address : {"id":25,"first_name":"cjxjxxjc","last_name":null,"email":null,"phone":"9234567890","state":null,"city":null,"pin_code":"123456","type":null,"address":null,"created_at":"2020-06-27 11:19:05","updated_at":"2020-06-28 04:09:17","user_id":26,"defaults":0,"address_line1":"chcucuc","address_line2":"cjjcjci","alternate_phone":"9876543210","country":"India","area":"cjcvkvj","deleted_at":null}
         * order_date : 2020-07-10
         * expert_date : 2020-07-10
         * order_items : [{"name":"Soft Noodles","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/uNFxwc0gItVP9bbYu3XxA1njFK7leaEMPVe0CqwX.jpeg","quantity":7,"weight":"600 gm","price":"45.00","item_sub_total":"280.00"},{"name":"Bambino Vermicelli","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/lAeTlu3qv61ab0GQgkfrkxmobu7YzFlcMkdVcFms.png","quantity":15,"weight":"400 gm","price":"40.00","item_sub_total":"555.00"}]
         */

        private int order_id;
        private String grand_total;
        private int item_count;
        private String payment_method;
        private String sub_total;
        private String status;
        private AddressBean address;
        private String order_date;
        private String expert_date;
        private List<OrderItemsBean> order_items;

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public String getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(String grand_total) {
            this.grand_total = grand_total;
        }

        public int getItem_count() {
            return item_count;
        }

        public void setItem_count(int item_count) {
            this.item_count = item_count;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getSub_total() {
            return sub_total;
        }

        public void setSub_total(String sub_total) {
            this.sub_total = sub_total;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public AddressBean getAddress() {
            return address;
        }

        public void setAddress(AddressBean address) {
            this.address = address;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getExpert_date() {
            return expert_date;
        }

        public void setExpert_date(String expert_date) {
            this.expert_date = expert_date;
        }

        public List<OrderItemsBean> getOrder_items() {
            return order_items;
        }

        public void setOrder_items(List<OrderItemsBean> order_items) {
            this.order_items = order_items;
        }

        public static class AddressBean {
            /**
             * id : 25
             * first_name : cjxjxxjc
             * last_name : null
             * email : null
             * phone : 9234567890
             * state : null
             * city : null
             * pin_code : 123456
             * type : null
             * address : null
             * created_at : 2020-06-27 11:19:05
             * updated_at : 2020-06-28 04:09:17
             * user_id : 26
             * defaults : 0
             * address_line1 : chcucuc
             * address_line2 : cjjcjci
             * alternate_phone : 9876543210
             * country : India
             * area : cjcvkvj
             * deleted_at : null
             */

            private int id;
            private String first_name;
            private Object last_name;
            private Object email;
            private String phone;
            private Object state;
            private Object city;
            private String pin_code;
            private Object type;
            private Object address;
            private String created_at;
            private String updated_at;
            private int user_id;
            private int defaults;
            private String address_line1;
            private String address_line2;
            private String alternate_phone;
            private String country;
            private String area;
            private Object deleted_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public Object getLast_name() {
                return last_name;
            }

            public void setLast_name(Object last_name) {
                this.last_name = last_name;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public Object getState() {
                return state;
            }

            public void setState(Object state) {
                this.state = state;
            }

            public Object getCity() {
                return city;
            }

            public void setCity(Object city) {
                this.city = city;
            }

            public String getPin_code() {
                return pin_code;
            }

            public void setPin_code(String pin_code) {
                this.pin_code = pin_code;
            }

            public Object getType() {
                return type;
            }

            public void setType(Object type) {
                this.type = type;
            }

            public Object getAddress() {
                return address;
            }

            public void setAddress(Object address) {
                this.address = address;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public int getDefaults() {
                return defaults;
            }

            public void setDefaults(int defaults) {
                this.defaults = defaults;
            }

            public String getAddress_line1() {
                return address_line1;
            }

            public void setAddress_line1(String address_line1) {
                this.address_line1 = address_line1;
            }

            public String getAddress_line2() {
                return address_line2;
            }

            public void setAddress_line2(String address_line2) {
                this.address_line2 = address_line2;
            }

            public String getAlternate_phone() {
                return alternate_phone;
            }

            public void setAlternate_phone(String alternate_phone) {
                this.alternate_phone = alternate_phone;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }
        }

        public static class OrderItemsBean {
            /**
             * name : Soft Noodles
             * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/uNFxwc0gItVP9bbYu3XxA1njFK7leaEMPVe0CqwX.jpeg
             * quantity : 7
             * weight : 600 gm
             * price : 45.00
             * item_sub_total : 280.00
             */

            private String name;
            private String image;
            private int quantity;
            private String weight;
            private String price;
            private String item_sub_total;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public String getWeight() {
                return weight;
            }

            public void setWeight(String weight) {
                this.weight = weight;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getItem_sub_total() {
                return item_sub_total;
            }

            public void setItem_sub_total(String item_sub_total) {
                this.item_sub_total = item_sub_total;
            }
        }
    }
}
