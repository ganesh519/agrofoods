package com.sample.agrofoods.Models;

public class Product {
    private String id;
    private int product_id;
    private String product_name;
    private String mrp_price;
    private String discount_price;
    private int purchase_quantity;
    private String images;
    private String Quantity;
    private String profit;



    public Product(String id, int product_id, String product_name, String mrp_price, int purchase_quantity,String images,String discount_price,String Quantity,String profit) {
        this.id = id;
        this.product_id = product_id;
        this.product_name = product_name;
        this.mrp_price = mrp_price;
        this.purchase_quantity = purchase_quantity;
        this.images = images;
        this.discount_price=discount_price;
        this.Quantity=Quantity;
        this.profit=profit;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }


    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public int getPurchase_quantity() {
        return purchase_quantity;
    }

    public void setPurchase_quantity(int purchase_quantity) {
        this.purchase_quantity = purchase_quantity;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
