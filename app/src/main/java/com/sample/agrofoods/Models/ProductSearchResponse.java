package com.sample.agrofoods.Models;

import java.util.List;

public class ProductSearchResponse {

    /**
     * code : 204
     * message : Products data
     * data : [{"product_id":56,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/u0Sv8dvaWc5YRSQC9QKf27JWuRkD4kbslf2oObvL.jpeg","name":"Rock Salt / मोटा नमक/  కల్లు ఉప్పు"},{"product_id":57,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/aaCe6fT8qkWEg2RpF2vKdw4oowz7h1in2dyur6Sl.jpeg","name":"Aashirvaad Iodised Salt/आशिरवाद आयोडाइज्ड नमक/ఆశిర్వాడ్ ఉప్పు"},{"product_id":58,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/IDIDIuPDKVn23x957zFjP7qAHJyrDEj7KcVkUR9Z.jpeg","name":"Tata Iodised Salt/ टाटा आयोडाइज्ड नमक/టాటా ఉప్పు"},{"product_id":123,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/J7h4MJWbbePtAgIeloXjfQJA8djna6vqVxlxgokX.jpeg","name":"Roasted & Salted Pista / पिस्ता / పిస్తా"},{"product_id":128,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/Sah2cIzwcAVgK6e8EVrKZKrHHf1PECjqFncK65Dd.png","name":"Colgate Active Salt Toothpaste"}]
     */

    private int code;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * product_id : 56
         * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/u0Sv8dvaWc5YRSQC9QKf27JWuRkD4kbslf2oObvL.jpeg
         * name : Rock Salt / मोटा नमक/  కల్లు ఉప్పు
         */

        private int product_id;
        private String image;
        private String name;

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
