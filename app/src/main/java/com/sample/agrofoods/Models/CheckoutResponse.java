package com.sample.agrofoods.Models;

import java.util.List;

public class CheckoutResponse {

    /**
     * code : 200
     * message : Checkout data
     * data : {"addres":[{"address_id":15,"name":"ganesh","phone":"9347432387","email":null,"address_line1":"2-78","address_line2":"ganeshnagar","alternate_phone":"9876543210","state":"telangana","city":"hyderabad","area":"kukatpally","pincode":"500606","address":null}],"payment_methods":[{"cod":"Cash On Delivery"}],"delivery_charges":0,"sub_total":0,"grand_total":0}
     */

    private int code;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * addres : [{"address_id":15,"name":"ganesh","phone":"9347432387","email":null,"address_line1":"2-78","address_line2":"ganeshnagar","alternate_phone":"9876543210","state":"telangana","city":"hyderabad","area":"kukatpally","pincode":"500606","address":null}]
         * payment_methods : [{"cod":"Cash On Delivery"}]
         * delivery_charges : 0
         * sub_total : 0
         * grand_total : 0
         */

        private int delivery_charges;
        private int sub_total;
        private int grand_total;
        private List<AddresBean> addres;
        private List<PaymentMethodsBean> payment_methods;

        public int getDelivery_charges() {
            return delivery_charges;
        }

        public void setDelivery_charges(int delivery_charges) {
            this.delivery_charges = delivery_charges;
        }

        public int getSub_total() {
            return sub_total;
        }

        public void setSub_total(int sub_total) {
            this.sub_total = sub_total;
        }

        public int getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(int grand_total) {
            this.grand_total = grand_total;
        }

        public List<AddresBean> getAddres() {
            return addres;
        }

        public void setAddres(List<AddresBean> addres) {
            this.addres = addres;
        }

        public List<PaymentMethodsBean> getPayment_methods() {
            return payment_methods;
        }

        public void setPayment_methods(List<PaymentMethodsBean> payment_methods) {
            this.payment_methods = payment_methods;
        }

        public static class AddresBean {
            /**
             * address_id : 15
             * name : ganesh
             * phone : 9347432387
             * email : null
             * address_line1 : 2-78
             * address_line2 : ganeshnagar
             * alternate_phone : 9876543210
             * state : telangana
             * city : hyderabad
             * area : kukatpally
             * pincode : 500606
             * address : null
             */

            private int address_id;
            private String name;
            private String phone;
            private Object email;
            private String address_line1;
            private String address_line2;
            private String alternate_phone;
            private String state;
            private String city;
            private String area;
            private String pincode;
            private Object address;

            public int getAddress_id() {
                return address_id;
            }

            public void setAddress_id(int address_id) {
                this.address_id = address_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public String getAddress_line1() {
                return address_line1;
            }

            public void setAddress_line1(String address_line1) {
                this.address_line1 = address_line1;
            }

            public String getAddress_line2() {
                return address_line2;
            }

            public void setAddress_line2(String address_line2) {
                this.address_line2 = address_line2;
            }

            public String getAlternate_phone() {
                return alternate_phone;
            }

            public void setAlternate_phone(String alternate_phone) {
                this.alternate_phone = alternate_phone;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public Object getAddress() {
                return address;
            }

            public void setAddress(Object address) {
                this.address = address;
            }
        }

        public static class PaymentMethodsBean {
            /**
             * cod : Cash On Delivery
             */

            private String cod;

            public String getCod() {
                return cod;
            }

            public void setCod(String cod) {
                this.cod = cod;
            }
        }
    }
}
