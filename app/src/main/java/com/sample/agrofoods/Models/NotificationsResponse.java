package com.sample.agrofoods.Models;

import java.util.List;

public class NotificationsResponse {

    /**
     * code : 200
     * message : Notifications
     * data : [{"title":"Offer","message":"Hi This is new offer for you .Buy Rs.500  you get Rs.50 Offer"}]
     */

    private int code;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : Offer
         * message : Hi This is new offer for you .Buy Rs.500  you get Rs.50 Offer
         */

        private String title;
        private String message;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
