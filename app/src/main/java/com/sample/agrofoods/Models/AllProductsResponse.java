package com.sample.agrofoods.Models;

import java.util.List;

public class AllProductsResponse {

    /**
     * code : 200
     * message : Category Products
     * data : [{"id":56,"name":"Rock Salt / मोटा नमक/  కల్లు ఉప్పు","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/u0Sv8dvaWc5YRSQC9QKf27JWuRkD4kbslf2oObvL.jpeg","weight":"1","price":"15.00","discount":"12.00","category":"Salt, Sugar & Jaggery"},{"id":57,"name":"Aashirvaad Iodised Salt/आशिरवाद आयोडाइज्ड नमक/ఆశిర్వాడ్ ఉప్పు","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/aaCe6fT8qkWEg2RpF2vKdw4oowz7h1in2dyur6Sl.jpeg","weight":"1","price":"20.00","discount":"18.00","category":"Salt, Sugar & Jaggery"},{"id":58,"name":"Tata Iodised Salt/ टाटा आयोडाइज्ड नमक/టాటా ఉప్పు","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/IDIDIuPDKVn23x957zFjP7qAHJyrDEj7KcVkUR9Z.jpeg","weight":"1","price":"20.00","discount":"18.00","category":"Salt, Sugar & Jaggery"},{"id":59,"name":"Sugar refined/ चीनी / చక్కెర","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/s3vVqdzbtF9kXIKVxHEsx4gUT7IW3hIlx12EyoCE.jpeg","weight":"1","price":"40.00","discount":"38.00","category":"Salt, Sugar & Jaggery"},{"id":60,"name":"Jaggery - Blocks /  गुड़/  బెల్లం","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/DgPLv9dTq8GHyNdIeMycIpmIFUoSVb3H6ggFRULG.png","weight":"1","price":"70.00","discount":"60.00","category":"Salt, Sugar & Jaggery"},{"id":61,"name":"Jaggery - Round / गुड़/  బెల్లం","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/aFxQqFNt2x2MwVmPYdRGCgwPzifl4gGISu48lyvl.jpeg","weight":"1","price":"70.00","discount":"60.00","category":"Salt, Sugar & Jaggery"}]
     */

    private int code;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 56
         * name : Rock Salt / मोटा नमक/  కల్లు ఉప్పు
         * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/u0Sv8dvaWc5YRSQC9QKf27JWuRkD4kbslf2oObvL.jpeg
         * weight : 1
         * price : 15.00
         * discount : 12.00
         * category : Salt, Sugar & Jaggery
         */

        private int id;
        private String name;
        private String image;
        private String weight;
        private String price;
        private String discount;
        private String category;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }
    }
}
