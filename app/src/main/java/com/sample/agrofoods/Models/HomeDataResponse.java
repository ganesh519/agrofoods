package com.sample.agrofoods.Models;

import java.util.List;

public class HomeDataResponse {


    /**
     * code : 200
     * message : home data
     * data : {"banners":[{"name":"Order & Get Free Home Delivery","link":null,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/banners/fjd0brxkDczlBeI6nBwFBuTcAP8gs7IUGlqkTV5Q.jpeg"},{"name":"Up To 10% Off","link":null,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/banners/dbdFxevOACougCyqHsE2WyLivAgye7HWGPFt3Lqp.jpeg"},{"name":"Free 1 Kg Sugar","link":null,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/banners/ctk038XSklnRRLj6BFRZedege5T8YoxyybDeuqzT.jpeg"},{"name":".Organic & Fresh","link":null,"image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/banners/wl728bSmVXuNOpSnGoBCjOtaxn1TW5sfYSY56ilr.jpeg"}],"categories":[{"id":31,"name":"Books & Stationary","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/AuPEg14ZcsiqlnzggXHANqGi92425hu8rIgjb11f.jpeg","sub_categorie":0},{"id":32,"name":"Cleaning And Household","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/rYtURK8WbRsrsbZUstNAbdFjAYq3aXV19jn5mLl4.jpeg","sub_categorie":1},{"id":36,"name":"Fruits and Veggies","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/IJmGHC78SSOur7gnxwhSjutokqokRZW0QhKHKiOy.jpeg","sub_categorie":0},{"id":41,"name":"Other Household & Home accessories","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/8Ki5riuYUIYMc5n983xTR4pjEOXpJFflcjsfDa85.jpeg","sub_categorie":0},{"id":42,"name":"Personal Care","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/RJIYm4r7MHxK5Fd6BbT7hu1KaBG1lHqq0uQMbk0W.jpeg","sub_categorie":1},{"id":43,"name":"Pooja Needs","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/L0OOxFluELrayesFeUX574RUEplSwXgXVOClgX58.jpeg","sub_categorie":0},{"id":46,"name":"Biscuits Namkeen & Snacks","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/fxeqpkeg8MC92htpLrBiYVup9V0mZ02D6DjOkOdH.jpeg","sub_categorie":1},{"id":48,"name":"Tea And Beverages","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/h1B2priSSVqWRaYzYIheAHHR14SzPlNoDrnGU09J.jpeg","sub_categorie":1},{"id":56,"name":"Rice , Oils Atta, & Foodgrains","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/Z87ZjCrzWpsvWS98BHORW1bBQKK39V9lqsXPAiFg.jpeg","sub_categorie":1}],"products":[{"id":298,"name":"Soft Noodles","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/uNFxwc0gItVP9bbYu3XxA1njFK7leaEMPVe0CqwX.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":292,"name":"Bambino Vermicelli","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/lAeTlu3qv61ab0GQgkfrkxmobu7YzFlcMkdVcFms.png","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":291,"name":"Sunfeast Yipee Noodles - Magic Masala","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/VOg5PTXZarK9qIfBlgDq1RhjVTpIkhpY1PMzzxuP.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":290,"name":"Maggi 2-Minute Instant Noodles","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/6idjlJX6ZJM1uEikbxT14w4j62hX8JHP8FkGsxKK.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":289,"name":"Horlicks Women's Horlicks Health & Nutrition Drink - Caramel Flavour, No Added Sugar","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/Vjk2YC0mCbT3PGbHWJflA0Y2p2gsvMIQJvmrYFGu.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":288,"name":"Complan Nutrition & Health Drink - Royale Chocolate","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/SQDQAyWHL8ilJnCuOPcYOfQloXnd9Y6U8TsxrdUh.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":287,"name":"Pediasure Nutritional Powder - Vanilla Delight","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/4X7h51jPBiSxzndT0J57mQrLPgFJ0a1H6oaYQ0dy.png","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":286,"name":"Boost Nutrition Drink - Health, Energy & Sports","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/IpH862is6skBqviIbgADUifm3AkMP6CujTRGRDKb.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":285,"name":"Bournvita - Chocolate Health Drink","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/TG6oJkYB6H3EEhXJ2WksjbICgEpI6aN1Ufn9JPGc.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":284,"name":"Horlicks Health & Nutrition Drink - Classic Malt","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/gjXR8IDjr3m8FWt2VuLTxgh2La6fVVZGtSDwAOnW.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":283,"name":"Nescafe Sunrise Instant Coffee - Chicory Mixture","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/dm5h2f75SVi7QgaPuIJV82evcvU4wTTp3bHOgTFn.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":282,"name":"Bru Filter Coffee","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/NQ8fqTIj4hjUcSnh72BpSarTgc2IjaKDTQvZEaae.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":281,"name":"Bru Instant Coffee","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/IVsTjGOd0OX1TYnOoKsePBfalVnjlbX0QWj8fAkn.png","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":280,"name":"Tata Tetley  Lemon & Honey Green Tea Bags(30 pcs)","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/69PHwKxK45SHMraDJEZMRSouhruxK7TyJjwvQsgl.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":279,"name":"Lipton Honey Lemon Green Tea Bags(25 pcs)","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/jqMAjjlIp0T6hPRS7PEORsHR1Msg93Q7q3OxsX7Z.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1},{"id":278,"name":"Tata Tea Gemini Tea","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/AQ0Yz3Uj7I9qNuxBjy5p8Dx06CrPnYZjZ0FYqn1U.jpeg","price":null,"ratings":3,"reviews":0,"isAddedToCart":false,"isAddedBtn":false,"isFavourite":false,"quantity":1}]}
     */

    private int code;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<BannersBean> banners;
        private List<CategoriesBean> categories;
        private List<ProductsBean> products;

        public List<BannersBean> getBanners() {
            return banners;
        }

        public void setBanners(List<BannersBean> banners) {
            this.banners = banners;
        }

        public List<CategoriesBean> getCategories() {
            return categories;
        }

        public void setCategories(List<CategoriesBean> categories) {
            this.categories = categories;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class BannersBean {
            /**
             * name : Order & Get Free Home Delivery
             * link : null
             * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/banners/fjd0brxkDczlBeI6nBwFBuTcAP8gs7IUGlqkTV5Q.jpeg
             */

            private String name;
            private Object link;
            private String image;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getLink() {
                return link;
            }

            public void setLink(Object link) {
                this.link = link;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }

        public static class CategoriesBean {
            /**
             * id : 31
             * name : Books & Stationary
             * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/AuPEg14ZcsiqlnzggXHANqGi92425hu8rIgjb11f.jpeg
             * sub_categorie : 0
             */

            private int id;
            private String name;
            private String image;
            private int sub_categorie;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public int getSub_categorie() {
                return sub_categorie;
            }

            public void setSub_categorie(int sub_categorie) {
                this.sub_categorie = sub_categorie;
            }
        }

        public static class ProductsBean {
            /**
             * id : 298
             * name : Soft Noodles
             * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/products/uNFxwc0gItVP9bbYu3XxA1njFK7leaEMPVe0CqwX.jpeg
             * price : null
             * ratings : 3
             * reviews : 0
             * isAddedToCart : false
             * isAddedBtn : false
             * isFavourite : false
             * quantity : 1
             */

            private int id;
            private String name;
            private String image;
            private Object price;
            private int ratings;
            private int reviews;
            private boolean isAddedToCart;
            private boolean isAddedBtn;
            private boolean isFavourite;
            private int quantity;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Object getPrice() {
                return price;
            }

            public void setPrice(Object price) {
                this.price = price;
            }

            public int getRatings() {
                return ratings;
            }

            public void setRatings(int ratings) {
                this.ratings = ratings;
            }

            public int getReviews() {
                return reviews;
            }

            public void setReviews(int reviews) {
                this.reviews = reviews;
            }

            public boolean isIsAddedToCart() {
                return isAddedToCart;
            }

            public void setIsAddedToCart(boolean isAddedToCart) {
                this.isAddedToCart = isAddedToCart;
            }

            public boolean isIsAddedBtn() {
                return isAddedBtn;
            }

            public void setIsAddedBtn(boolean isAddedBtn) {
                this.isAddedBtn = isAddedBtn;
            }

            public boolean isIsFavourite() {
                return isFavourite;
            }

            public void setIsFavourite(boolean isFavourite) {
                this.isFavourite = isFavourite;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }
        }
    }
}
