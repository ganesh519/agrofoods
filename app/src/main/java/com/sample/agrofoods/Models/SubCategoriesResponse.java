package com.sample.agrofoods.Models;

import java.util.List;

public class SubCategoriesResponse {


    /**
     * code : 200
     * data : [{"id":30,"name":"Bathing Bars & Handwash","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/S8rxpIxn17fwpas5hvT4TUs5myADM9nG5n0WZlVc.jpeg","sub_sub_categorie":0},{"id":35,"name":"Feminine Hygiene","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/bKX9oyVBTdSczQoIqZoYFyMQBUxqkFPoBJD7pxCc.jpeg","sub_sub_categorie":1},{"id":38,"name":"Hair Care","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/CnXnS0l5CBDXZwWcB3WQWYCbwrhfv4hOnjPyHYTe.jpeg","sub_sub_categorie":1},{"id":40,"name":"Oral Care","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/5majDuH69yogNhjF7DsXEFXouRW2Py9sOhrXOyzJ.jpeg","sub_sub_categorie":1},{"id":45,"name":"Skin Care & Body Care","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/Yx9Z20NF5zFQsvcKXnmQm1Y9hnRQMCAHwb5CCCJ7.jpeg","sub_sub_categorie":1},{"id":62,"name":"Health & Wellness","image":"https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/2hEgBnhoNJj7fkLkrmSJuzh8DF8BZofPnRaDb80q.jpeg","sub_sub_categorie":0}]
     */

    private int code;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 30
         * name : Bathing Bars & Handwash
         * image : https://fimgphp.s3.ap-south-1.amazonaws.com/upload/images/categories/S8rxpIxn17fwpas5hvT4TUs5myADM9nG5n0WZlVc.jpeg
         * sub_sub_categorie : 0
         */

        private int id;
        private String name;
        private String image;
        private int sub_sub_categorie;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getSub_sub_categorie() {
            return sub_sub_categorie;
        }

        public void setSub_sub_categorie(int sub_sub_categorie) {
            this.sub_sub_categorie = sub_sub_categorie;
        }
    }
}
