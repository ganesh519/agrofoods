package com.sample.agrofoods.Models;

public class LocalServiceResponse {
    int image;
    String name,mobileno,service,altermno,desc;

    public LocalServiceResponse(int image,String name,String mobileno,String service,String altermno,String desc){
        this.image=image;
        this.name=name;
        this.mobileno=mobileno;
        this.service=service;
        this.altermno=altermno;
        this.desc=desc;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobile) {
        this.mobileno = mobile;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getAltermno() {
        return altermno;
    }

    public void setAltermno(String alter) {
        this.altermno = alter;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
