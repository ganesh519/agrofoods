package com.sample.agrofoods.Models;

public class LoginResponse {


    /**
     * code : 200
     * message : Otp Send successfully
     * otp : 898618
     */

    private int code;
    private String message;
    private int otp;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }
}
