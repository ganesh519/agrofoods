package com.sample.agrofoods.Models;

import java.util.List;

public class RegistrationResponse {


    /**
     * code : 409
     * message : The email has already been taken.
     */

    private int code;
    private String message;
    private List<RegistrationResponse.DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RegistrationResponse.DataBean> getData() {
        return data;
    }

    public void setData(List<RegistrationResponse.DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : raju
         * phone : 9347432387
         * token : 1QnTjeXmXS2WhKz8Kt1Q6EjK5wy2MpNa
         */

        private String name;
        private String phone;
        private String token;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
