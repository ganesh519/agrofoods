package com.sample.agrofoods.Models;

public class SpinnerModel {
    String id,name,price,discountprice,in_cart,offer;

    public SpinnerModel(String id, String name, String price, String discountprice,String in_cart,String offer) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.discountprice = discountprice;
        this.in_cart=in_cart;
        this.offer=offer;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscountprice() {
        return discountprice;
    }

    public void setDiscountprice(String discountprice) {
        this.discountprice = discountprice;
    }

    public String getIn_cart() {
        return in_cart;
    }

    public void setIn_cart(String in_cart) {
        this.in_cart = in_cart;
    }
}
